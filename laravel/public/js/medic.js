$(document).ready(function () {

 	// script for smooth scrolling //
 	jQuery(document).ready(function ($) {
 		$('body').on('click', '.scroll', function (event) {
 			event.preventDefault();

 			$('html,body').animate({
 				scrollTop: $(this.hash).offset().top
 			}, 1000);
 		});
 	});

 	//script  for  ease //
 	$(document).ready(function () {
 		/*
 		 var defaults = {
 			 containerID: 'toTop', // fading element id
 			 containerHoverID: 'toTopHover', // fading element hover id
 			 scrollSpeed: 1200,
 			 easingType: 'linear' 
 		 };
 		 */

 		$().UItoTop({
 			easingType: 'easeOutQuart'
 		});

 	});

  // Fixed nav
  // $('a[href^="#"]').click(function (event) {
  //     var id = $(this).attr("href");
  //     var target = $(id).offset().top;
  //     $('html, body').animate({
  //         scrollTop: target
  //     }, 500);
  //     event.preventDefault();
  // });

  var offset = $('nav').offset().top;
  $(window).scroll(function () {
      if ($(this).scrollTop() >= offset) {
          $('nav').addClass('isFixed');
          $('html').addClass('whiteSpace');
      } else {
          $('nav').removeClass('isFixed');
          $('html').removeClass('whiteSpace');
      }
  });

});