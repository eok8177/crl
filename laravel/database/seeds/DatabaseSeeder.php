<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Evgen',
            'email' => 'eok8177@gmail.com',
            'password' => bcrypt('secret'),
            'role' => 'admin',
        ]);
        DB::table('users')->insert([
            'name' => 'User',
            'email' => 'eok1877@gmail.com',
            'password' => bcrypt('secret'),
            'role' => 'user',
        ]);
    }
}
