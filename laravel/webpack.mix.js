const mix = require('laravel-mix');

mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css/style.min.css')
   // .styles('public/css/style.css', 'public/css/style.min.css')
;

mix.options({
    processCssUrls: false
});

mix.webpackConfig({
   resolve: {
      extensions: ['.js', '.vue', '.json'],
      alias: {
         'vue$': 'vue/dist/vue.esm.js',
         '@': __dirname + '/resources/js'
      },
   }
});