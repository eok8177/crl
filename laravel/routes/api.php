<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['namespace' => 'Api'], function() {

    Route::get('/categories',  ['uses' => 'PageController@categories']);
    Route::get('/category/{slug}',  ['uses' => 'PageController@category']);
    Route::get('/page/{slug}',  ['uses' => 'PageController@page']);
    Route::get('/last/',  ['uses' => 'PageController@last']);

    Route::get('/doctors',  ['uses' => 'DoctorController@index']);

    Route::get('/home',  ['uses' => 'HomeController@index']);

});