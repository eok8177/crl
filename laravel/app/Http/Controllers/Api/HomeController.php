<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Model\Slide;
use App\Model\Page;

class HomeController extends Controller
{
    public function index()
    {
        $slides = Slide::where('published', 1)
            ->orderBy('order', 'asc')
            ->get();

        $blog = Page::where('published', 1)
            ->where('category_id', 3) //Поради лікарів
            ->orderBy('created_at', 'desc')
            ->limit(3)
            ->get();

        return response()->json([
            'slides' => $slides,
            'blog' => $blog
        ], 200);
    }

}
