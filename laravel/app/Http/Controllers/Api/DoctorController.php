<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Model\Doctor;


class DoctorController extends Controller
{
    public function index()
    {

        $res = Doctor::where('published', 1)
            ->orderBy('id', 'asc')
            ->get();
        return response()->json($res, 200);
    }

    public function item($slug)
    {
        $item = Doctor::where('slug', $slug)->where('published', 1)->first();

        if(!$item)
            return response()->json(['error'=> 'not found'], 400);

        return response()->json($item, 200);
    }

}
