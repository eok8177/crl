<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Model\Page;
use App\Model\PageCategory;


class PageController extends Controller
{
    public function categories()
    {

        $res = PageCategory::where('published', 1)
            ->orderBy('id', 'asc')
            ->get();
        return response()->json($res, 200);
    }


    public function category($slug)
    {
        $page = PageCategory::with('pages')->where('slug', $slug)->where('published', 1)->first();

        if(!$page)
            return response()->json(['error'=> 'not found'], 400);

        return response()->json($page, 200);
    }


    public function page($slug)
    {
        $page = Page::with('category')->where('slug', $slug)->where('published', 1)->first();

        if(!$page)
            return response()->json(['error'=> 'not found'], 400);

        $last = Page::where('category_id', $page->category_id)
            ->where('id', '!=' , $page->id)
            ->where('published', 1)
            ->orderBy('id', 'desc')
            ->limit(5)
            ->get();

        return response()->json([
            'page' => $page,
            'last' => $last
        ], 200);
    }

    public function last()
    {
        $pages = Page::with('category')
            ->where('published', 1)
            ->limit(3)
            ->orderBy('id', 'desc')
            ->get();

        if(!$pages)
            return response()->json(['error'=> 'not found'], 400);

        return response()->json($pages, 200);
    }

}
