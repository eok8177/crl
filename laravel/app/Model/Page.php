<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Page extends Model
{
    protected $table = 'pages';

    protected $guarded = [];

    use Sluggable;

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function category()
    {
        return $this->belongsTo(PageCategory::class, 'category_id');
    }

    public function categories()
    {
        return PageCategory::pluck('title', 'id')->all();
    }
}
