@extends('admin.layout')

@section('content')
<div class="card">
  <div class="card-header bg-light">
    <h3>@lang('message.slide')</h3>
  </div>

  <div class="card-body">

    {!! Form::open(['route' => ['admin.slide.store'], 'method' => 'POST']) !!}
      @include('admin.slide.form')
    {!! Form::close() !!}

  </div>
</div>

@endsection
