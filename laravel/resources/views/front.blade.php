<!DOCTYPE html>
<html lang="ua">

<head>
  <title>Гопри ЦРЛ | Головна</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta charset="UTF-8" />
  <base href="/">

  <link rel="stylesheet" href="css/style.min.css" type="text/css" media="all" />

  <link rel="stylesheet" href="css/fontawesome-all.css">

  <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
  <link href="//fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">

</head>

<body>

  <div id="app"></div>


  <script src="js/app.js"></script>

  <script src="js/jquery-2.2.3.min.js"></script>

  <!-- smooth scrolling -->
  <script src="js/SmoothScroll.min.js"></script>
  <!-- move-top -->
  <script src="js/move-top.js"></script>
  <!-- easing -->
  <script src="js/easing.js"></script>
  <!--  necessary snippets for few javascript files -->
  <script src="js/medic.js"></script>

  <script src="js/bootstrap.js"></script>
  <!-- Necessary-JavaScript-File-For-Bootstrap -->

</body>

</html>