import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

import Home from '@/views/Home'
import About from '@/views/About'
import Contact from '@/views/Contact'
import Gallery from '@/views/Gallery'
import Post from '@/views/Post'
import Category from '@/views/Category'
import Page from '@/views/Page'
// import ErrorPage from '@/views/ErrorPage'

const routes = [

  {path: '/', name: 'Home', component: Home},
  {path: '/about-us', name: 'About', component: About},
  {path: '/contact', name: 'Contact', component: Contact},
  {path: '/gallery', name: 'Gallery', component: Gallery},
  {path: '/category/:slug', name: 'Category', component: Category, props: true},
  {path: '/page/:slug', name: 'Page', component: Page, props: true},
  {path: '/post', name: 'Post', component: Post},
  // {path: '/404', name: '404', component: ErrorPage},
  // {path: '/:slug', name: 'Page', component: Page, props: true},

  // { path: '*', redirect: '/404', hidden: true }
]

export default new Router({
  mode: 'history',
  routes: routes,
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  }
})


